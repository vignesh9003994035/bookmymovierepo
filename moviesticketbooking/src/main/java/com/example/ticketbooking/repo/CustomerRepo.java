package com.example.ticketbooking.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ticketbooking.model.CustomerDetails;

@Repository
public interface CustomerRepo extends JpaRepository<CustomerDetails, Integer> {

	public CustomerDetails findByuserName(String userName);

	public CustomerDetails findByUserId(Integer userId);

}
