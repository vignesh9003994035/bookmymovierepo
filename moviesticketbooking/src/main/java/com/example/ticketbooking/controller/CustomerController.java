package com.example.ticketbooking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ticketbooking.dto.CustomerDto;
import com.example.ticketbooking.dto.CustomerLoginDto;
import com.example.ticketbooking.service.CustomerService;
import com.example.ticketbooking.util.ResponseData;

@RestController
@RequestMapping("/customer")

public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@PostMapping("/signup")
	public ResponseData<String> saveUserData(@RequestBody CustomerDto customerDto) {

		return customerService.saveCustomerData(customerDto);

	}

	@PostMapping("/login")
	public ResponseData<Integer> userLogin(@RequestBody CustomerLoginDto customerLoginDto) {

		return customerService.userLogin(customerLoginDto);
	}

}
