package com.example.ticketbooking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.ticketbooking.dto.CustomerDto;
import com.example.ticketbooking.dto.CustomerLoginDto;
import com.example.ticketbooking.model.CustomerDetails;
import com.example.ticketbooking.repo.CustomerRepo;
import com.example.ticketbooking.util.MoviesTicketBookingContent;
import com.example.ticketbooking.util.ResponseData;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepo customerRepo;

	public ResponseData<String> saveCustomerData(CustomerDto customerDto) {
		ResponseData<String> response = new ResponseData<String>();
		CustomerDetails customer = customerRepo.findByuserName(customerDto.getUserName());
		if (customer == null) {
			CustomerDetails Data = new CustomerDetails();
			Data.setUserName(customerDto.getUserName());
			Data.setFullname(customerDto.getFullName());
			Data.setAge(customerDto.getAge());
			Data.setEmailId(customerDto.getEmailId());
			Data.setGender(customerDto.getGender());
			Data.setPhoneNumber(customerDto.getPhoneNumber());
			Data.setPassword(customerDto.getPassword());
			customerRepo.save(Data);
			response.setCode(HttpStatus.OK.value());
			response.setMessage(MoviesTicketBookingContent.USER_SIGNUP_SUCCESSFULLY);
		}

		else {

			response.setCode(HttpStatus.BAD_REQUEST.value());
			response.setMessage(MoviesTicketBookingContent.USER_NAME_EXIST);
		}
		return response;

	}

	public ResponseData<Integer> userLogin(CustomerLoginDto customerLoginDto) {
		ResponseData<Integer> response = new ResponseData<Integer>();
		CustomerDetails customer = customerRepo.findByuserName(customerLoginDto.getUserName());
		if (customer.getPassword().equals(customerLoginDto.getPassword())) {
			response.setData(customer.getUserId());
			response.setCode(HttpStatus.OK.value());
			response.setMessage(MoviesTicketBookingContent.LOGIN_SUCCESS);
		} else {

			response.setCode(HttpStatus.BAD_REQUEST.value());
			response.setMessage(MoviesTicketBookingContent.LOGIN_FAILURE_MESSAGE);
		}
		return response;
	}
}
