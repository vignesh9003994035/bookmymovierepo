package com.example.ticketbooking.util;

public class MoviesTicketBookingContent {
	
	public static final String FIELDS_EMPTY_MESSAGE = "Fields Missing";
	public static final String LOGIN_SUCCESS = "Logged in successfully";
	public static final String LOGIN_FAILURE_MESSAGE = "Credentials Incorrect, Try again";	
	public static final String USER_NOT_FOUND = "User not found";
	public static final String USER_SIGNUP_SUCCESSFULLY= "SUCCESSFULLY REGISTERED";	
	public static final String USER_NAME_EXIST= "User name already exists";

}
